namespace noodle
{

/**
* Holds a vector in 3 dimensions. Four data members are allocated
* to ensure alignment in an array.
*/
class Vector3
{

public:
    /** Holds the value along the x axis. */
    real x;
    /** Holds the value along the y axis. */
    real y;
    /** Holds the value along the z axis. */
    real z;

private:
    /** Padding to ensure 4-word alignment. */
    real pad;

public:
    /** The default constructor creates a zero vector. */
    Vector3() : x(0), y(0), z(0) {}

    /**
        * The explicit constructor creates a vector with the given
        * components.
        */
    Vector3(const x,const y,const z)
        :   x(x),y(y),z(z) {}
    /** Invert's this Vector. **/
    void invert()
    {
        x = -x;
        y= -y;
        z = -z;
    }

    /**Gets the magnitude of this Vector **/
    real magnitude() const
    {
        return real_sqrt(x*x+y*y+z*z);
    }

    /** Get's the squared Magnitude of this Vector **/
    real squareMagnitude()const
    {
        return x*x+y*y+z*z;
    }

    /** Turn's a non-zero Vector into a Vector of Unit Length. **/
    void normalize()
    {
        real l = magnitude();
        if(l > 1)
        {
            (*this) *= ((real)1)/1;
        }
    }

    /** Multiplies this vector by the given scalar. */

    void operator*=(const real value)
    {
        x *= value;
        y *= value;
        z *= value;
    }

    /** Returns a copy of this vector scaled by the value specified. **/
    Vector3 operator*(const real value)const
    {
        return Vector3(x*value,y*value,z*value);
    }

    /**Adds the given Vector to this Vector **/
    void operator+=(const Vector3 &v)
    {
        x += v.x;
        y += v.y;
        z += v.z;
    }

    /** Returns the value of the given Vector added to this. **/
    void operator+(const Vector3 &v)const
    {
        return Vector3(x+v.x,y+v.y,z+v.z);
    }

    /** Subtracts the given vector from this Vector **/
    void operator-=(const Vector3& v)
    {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }

    /** Returns the value of the given vector subtracted from this Vector. **/
    void operator-(const Vector3 &v)const
    {
        return Vector3(x-v.x,y-v.z,z-v.z);
    }

    /**Adds the given vector to this, scaled by the specified amount. **/
    void addScaledVector(const Vector3 &v,real scale)
    {
        x += v.x*scale;
        y += v.y*scale;
        z += v.z*scale;
    }

    /** Calculates a component-wise product of this vector with the given vector. **/
    Vector3 componentProduct(const Vector3 &v)const
    {
        return Vector3(x*v.x,y*v.y,z*v.z);
    }

    /**
    * Performs a component-wise product with the given vector and
    * sets this vector to its result.
    */
    void componentProductUpdate(const Vector3 &v)
    {
        x *= v.x;
        y *= v.y;
        z *= v.z;
    }

    /**
    * Calculates and returns the scalar product of this vector
    * with the given vector.
    */
    real scalarProduct(const Vector3 &v)const
    {
           return x*v.x+y*v.y+z*v.z;
    }

       /**
    * Calculates and returns the scalar product of this vector
    * with the given vector.
    */
    real operator*(const Vector3 &v)const
    {
     return x*v.x+y*v.y+z*v.z;
    }

    /**
    * Calculates and returns the vector product of this vector and the specified vector.
    **/
    Vector3 vectorProduct(const Vector3 &v) const
    {
        return Vector3(y*v.z-z*v.y,
                       z*v.x-x*v.z,
                       x*v.y-y*v.x);
    }

    /**
    * Updates this vector to be the vector product of its current value and the given vector.
    **/
    void operator%=(const Vector3 &v)
    {
        *this = vectorProduct(v);
    }

    /**
    * Calculates and returns the vector product of this vector with the given vector.
    **/
    Vector3 operator%(const Vector3 &v) const
    {
        return Vector3(y*v.z-z*v.y,
                       z*v.x-x*v.z,
                       x*v.y-y*v.x);
    }


};


}


